package com.mobile.c.model;

/**
 * Created by BohyunBartowski on 3/9/2018 0009.
 */

public class Status {
    public boolean isOnline;
    public long timestamp;

    public Status(){
        isOnline = false;
        timestamp = 0;
    }
}
