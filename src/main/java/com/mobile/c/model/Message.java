package com.mobile.c.model;

/**
 * Created by BohyunBartowski on 3/9/2018 0009.
 */

public class Message {
    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;
}
